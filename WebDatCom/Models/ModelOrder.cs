﻿namespace WebDatCom.Models
{
    public class ModelOrder
    {
        public string Note { get; set; }

        public DateTime CreatedAt { get; set; }

        public int TotalMoney { get; set; }

        public int UserId { get; set; }
    }
}
