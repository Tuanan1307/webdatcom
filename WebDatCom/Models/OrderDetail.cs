﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebDatCom.Models
{
    [Table("OrderDetail")]
    public class OrderDetail
    {

        public int OrderId { get; set; }

        public virtual Order Order { get; set; }

        public int FoodId { get; set; }

        public virtual Food Food { get; set; }

    }
}
