﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebDatCom.Models
{

    [Table("User")]
    public class User
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Accout { get; set; }

        public string PassWord { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
