﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebDatCom.Models
{

    [Table("Order")]
    public class Order
    {
        public int Id { get; set; }

        public string Note { get; set; }

        public DateTime CreatedAt { get; set; }

        public int TotalMoney { get; set; }

        //public string Checked { get; set; }

        //public string Sold { get; set; }

        public int UserId { get; set; }

        public virtual User User { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }

    }
}
