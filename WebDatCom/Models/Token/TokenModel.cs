﻿namespace WebDatCom.Models.Token
{
    public class TokenModel
    {
        public string AccsessToken { get; set; }

        public string RefreshToken { get; set; }
    }
}
