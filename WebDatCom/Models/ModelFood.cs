﻿namespace WebDatCom.Models
{
    public class ModelFood
    {

        public string FoodName { get; set; }

        public string Description { get; set; }

        public string Category { get; set; }

        public int Price { get; set; }

    }
}
