﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebDatCom.Models
{
    [Table("Food")]
    public class Food
    {
        public int Id { get; set; }

        public string FoodName { get; set; }


        public string Description { get; set; }

        public string Category { get; set; }

        public int Price { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
