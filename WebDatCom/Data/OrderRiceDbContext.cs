﻿using Microsoft.EntityFrameworkCore;
using WebDatCom.Models;
using WebDatCom.Models.Token;

namespace WebDatCom.Data
{
    public class OrderRiceDbContext : DbContext
    {
        public OrderRiceDbContext(DbContextOptions<OrderRiceDbContext> options)
             : base(options)
        {
            Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //user
            modelBuilder.Entity<User>(entity =>
            {
                entity.HasIndex(e => e.Accout).IsUnique();
                entity.Property(e => e.Name).IsRequired().HasMaxLength(50);
                entity.Property(e => e.Accout).IsRequired().HasMaxLength(50);
            });

            //OrderDetail
            modelBuilder.Entity<OrderDetail>()
            .HasKey(bc => new { bc.FoodId, bc.OrderId });

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Food> Foods { get; set; }
        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
    }
}
