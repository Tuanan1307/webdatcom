﻿
using Microsoft.AspNetCore.Mvc;
using WebDatCom.Models;
using WebDatCom.Models.Token;
using WebDatCom.Repository;

namespace WebDatCom.Controllers
{

    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IRepository<HandleLogin> _login;
        private readonly IRepository<HandleRegister> _register;
        private readonly IRepository<HandleUpdate> _update;
        private readonly IRepository<User> _user;
        private readonly IRepository<TokenModel> _token;


        public UserController(IRepository<HandleLogin> login, IRepository<HandleRegister> register, IRepository<HandleUpdate> update, IRepository<User> user, IRepository<TokenModel> token)
        {
            _login = login;
            _register = register;
            _update = update;
            _user = user;
            _token = token;

        }

        [HttpPost("login")]
        public async Task<IActionResult> Create(HandleLogin model)
        {

            var user = _login.Create(model);

            return Ok(user);

        }

        // register user
        [HttpPost("register")]
        public async Task<IActionResult> Register(HandleRegister model)
        {
            var user = _register.Create(model);

            return Ok(user);
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var users = _user.GetAll();

                return Ok(users);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        //update user
        [HttpPut("update")]
        public async Task<IActionResult> Update(int id, HandleUpdate model)
        {
            var user = _update.Update(id, model);

            return Ok(user);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {

            var user = _user.Get(id);
            return Ok(user);
        }

        [HttpPost("NewToken")]
        public async Task<IActionResult> RenewToken(TokenModel tokenModel)
        {
            var token = _token.Create(tokenModel);
            return Ok(token);

        }


    }
}
