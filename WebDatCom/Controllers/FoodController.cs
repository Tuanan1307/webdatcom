﻿using Microsoft.AspNetCore.Mvc;
using WebDatCom.Models;
using WebDatCom.Repository;

namespace WebDatCom.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FoodController : ControllerBase
    {

        private readonly IRepository<ModelFood> _foodrepository;

        public FoodController(IRepository<ModelFood> foodrepository)
        {
            _foodrepository = foodrepository;
        }


        [HttpPost("create")]
        public async Task<IActionResult> Create(ModelFood model)
        {
            try
            {
                var food = _foodrepository.Create(model);
                if (food == null)
                {
                    return BadRequest(new
                    {
                        Success = false,
                        Message = "create failed!",
                        Status = StatusCodes.Status404NotFound,
                    });

                }
                return Ok(new
                {
                    Success = true,
                    Message = "create successfully!",
                    Status = StatusCodes.Status200OK,
                    food,
                });
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var food = _foodrepository.Get(id);
                if (food == null)
                {
                    return NotFound(new
                    {
                        Success = false,
                        Message = "food not found!",
                        Status = StatusCodes.Status404NotFound,
                    });

                }
                return Ok(new
                {
                    Success = true,
                    Message = "successfully!",
                    Status = StatusCodes.Status200OK,
                    food,
                });
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var food = _foodrepository.GetAll();
                if (food == null)
                {
                    return NotFound(new
                    {
                        Success = false,
                        Message = "foods not found!"
                    });

                }
                return Ok(new
                {
                    Success = true,
                    Message = "successfully!",
                    Status = StatusCodes.Status200OK,
                    food,
                });
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var food = _foodrepository.Delete(id);
                if (food == null)
                {
                    return NotFound(new
                    {
                        Success = false,
                        Message = "food not found!",
                        Status = StatusCodes.Status404NotFound,
                    });

                }
                return Ok(new
                {
                    Success = true,
                    Message = "successfully!",
                    Status = StatusCodes.Status200OK,
                    food,
                });
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, ModelFood model)
        {
            try
            {
                var food = _foodrepository.Update(id, model);
                if (food == null)
                {
                    return NotFound(new
                    {
                        Success = false,
                        Message = "food not found!",
                        Status = StatusCodes.Status404NotFound,
                    });

                }
                return Ok(new
                {
                    Success = true,
                    Message = "successfully!",
                    Status = StatusCodes.Status200OK,
                    food,
                });
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

    }
}
