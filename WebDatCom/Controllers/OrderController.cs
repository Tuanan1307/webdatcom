﻿using Microsoft.AspNetCore.Mvc;
using WebDatCom.Models;
using WebDatCom.Repository;

namespace WebDatCom.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {

        private readonly IRepository<ModelOrder> _orderrepository;

        public OrderController(IRepository<ModelOrder> orderrepository)
        {
            _orderrepository = _orderrepository;
        }

        [HttpPost("order")]
        public async Task<IActionResult> Create(ModelOrder model)
        {
            try
            {
                var order = _orderrepository.Create(model);
                if (order == null)
                {
                    return BadRequest(new
                    {
                        Success = false,
                        Message = "create failed!",
                        Status = StatusCodes.Status404NotFound,
                    });
                }
                return Ok(new
                {
                    Success = true,
                    Message = "order successfully!",
                    Status = StatusCodes.Status200OK,
                    order,
                });
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var order = _orderrepository.GetAll();
                if (order == null)
                {
                    return NotFound(new
                    {
                        Success = false,
                        Message = "orders not found!"
                    });

                }
                return Ok(new
                {
                    Success = true,
                    Message = "successfully!",
                    Status = StatusCodes.Status200OK,
                    order,
                });
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var order = _orderrepository.Get(id);
                if (order == null)
                {
                    return NotFound(new
                    {
                        Success = false,
                        Message = "order not found!",
                        Status = StatusCodes.Status404NotFound,
                    });

                }
                return Ok(new
                {
                    Success = true,
                    Message = "successfully!",
                    Status = StatusCodes.Status200OK,
                    order,
                });
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
