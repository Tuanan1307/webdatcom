﻿using Microsoft.AspNetCore.Mvc;
using WebDatCom.Models;
using WebDatCom.Repository;

namespace WebDatCom.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderDetailController : ControllerBase
    {
        private readonly IRepository<ModelOrderDetail> _orderdetailrepository;

        public OrderDetailController(IRepository<ModelOrderDetail> orderdetailrepository)
        {
            _orderdetailrepository = orderdetailrepository;
        }

        [HttpPost("orderdetail")]
        public async Task<IActionResult> Create(ModelOrderDetail model)
        {
            try
            {
                var orderDetail = _orderdetailrepository.Create(model);
                if (orderDetail == null)
                {
                    return BadRequest(new
                    {
                        Success = false,
                        Message = "create failed!",
                        Status = StatusCodes.Status404NotFound,
                    });

                }
                return Ok(new
                {
                    Success = true,
                    Message = "create successfully!",
                    Status = StatusCodes.Status200OK,
                    orderDetail,
                });
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var orderDetail = _orderdetailrepository.GetAll();
                if (orderDetail == null)
                {
                    return NotFound(new
                    {
                        Success = false,
                        Message = "order detail not found!"
                    });

                }
                return Ok(new
                {
                    Success = true,
                    Message = "successfully!",
                    Status = StatusCodes.Status200OK,
                    orderDetail,
                });
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
