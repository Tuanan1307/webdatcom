﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using WebDatCom.Data;
using WebDatCom.Models;
using WebDatCom.Models.Token;

namespace WebDatCom.Repository
{
    public class UserRepository : IRepository<HandleLogin>, IRepository<HandleRegister>, IRepository<HandleUpdate>, IRepository<User>, IRepository<TokenModel>
    {
        private readonly OrderRiceDbContext _context;

        private readonly AppSettings _appSettings;

        public UserRepository(OrderRiceDbContext context, IOptionsMonitor<AppSettings> optionsMonitor)
        {
            _context = context;
            _appSettings = optionsMonitor.CurrentValue;
        }


        //login
        public object Create(HandleLogin model)
        {
            var user = _context.Users.SingleOrDefault(u => u.Accout == model.Accout && u.PassWord == GetPasswordHash(model.PassWord));
            if (user == null)
            {
                return new
                {
                    Success = false,
                    Message = "Invalid Accout/Passwork",
                    Status = StatusCodes.Status500InternalServerError,
                };

            }

            // cấp token
            // return Data
            var token = GenerateToken(user);
            return new
            {
                Success = true,
                Message = "login successfully!",
                Status = StatusCodes.Status200OK,
                token,
                user
            };
        }

        // hash password
        private string GetPasswordHash(string password)
        {
            using (var sha1 = new SHA1Managed())
            {
                var hash = Encoding.UTF8.GetBytes(password);
                var generatedHash = sha1.ComputeHash(hash);
                var generatedHashString = Convert.ToBase64String(generatedHash);
                return generatedHashString;
            }
        }

        public async Task<TokenModel> GenerateToken(User user)
        {
            var iwtTokenHandler = new JwtSecurityTokenHandler();

            var secretKyeBytes = Encoding.UTF8.GetBytes(_appSettings.SecretKey);

            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, user.Name),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim("Accout", user.Accout),
                    new Claim("Id", user.Id.ToString()),


                    //roles
                   // new Claim("TokenId", Guid.NewGuid().ToString()),
                }),
                Expires = DateTime.UtcNow.AddHours(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(secretKyeBytes), SecurityAlgorithms.HmacSha512Signature)

            };
            var token = iwtTokenHandler.CreateToken(tokenDescription);
            var accessToken = iwtTokenHandler.WriteToken(token);
            var refreshToken = GenerateRefreshToken();

            // lưu accessToken vào database
            var refreshTokenEntity = new RefreshToken
            {
                JwtId = token.Id,
                UserId = user.Id,
                Token = refreshToken,
                IsUsed = false,
                IsRevoked = false,
                IssuedAt = DateTime.UtcNow,
                ExpiredAt = DateTime.UtcNow.AddHours(1),

            };

            await _context.AddAsync(refreshTokenEntity);
            await _context.SaveChangesAsync();

            return new TokenModel
            {
                AccsessToken = accessToken,
                RefreshToken = refreshToken
            };

        }
        private string GenerateRefreshToken()
        {
            var random = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(random);
                return Convert.ToBase64String(random);
            }
        }

        //register
        public object Create(HandleRegister model)
        {
            var user = _context.Users.FirstOrDefault(u => u.Accout == model.Accout);
            if (user != null)
            {
                return new
                {
                    Success = false,
                    Message = "account already exists!",
                    Status = StatusCodes.Status500InternalServerError,
                };

            }
            user = new User
            {
                Accout = model.Accout,
                PassWord = GetPasswordHash(model.PassWord),
                Name = model.Name
            };
            _context.Add(user);
            _context.SaveChanges();
            //cấp token
            // return Data
            var token = GenerateToken(user);
            return new
            {
                Success = true,
                Message = "register successfully!",
                Status = StatusCodes.Status200OK,
                token,
                user
            };
        }

        //upadte
        public object Create(HandleUpdate t)
        {
            throw new NotImplementedException();
        }

        public object Delete(int id)
        {
            throw new NotImplementedException();
        }

        public object Get(int id)
        {
            return _context.Users.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<object> GetAll()
        {

            var users = _context.Users.ToList();
            return users;
        }

        public object Update(int id, HandleLogin t)
        {
            throw new NotImplementedException();
        }

        public object Update(int id, HandleRegister t)
        {
            throw new NotImplementedException();
        }

        public object Update(int id, HandleUpdate model)
        {
            var user = _context.Users.SingleOrDefault(x => x.Id == id);
            if (user == null)
            {
                return new
                {
                    Success = false,
                    Message = "user not found!",
                    Status = StatusCodes.Status500InternalServerError,
                };

            }
            user.PassWord = GetPasswordHash(model.PassWord);
            user.Name = model.Name;

            _context.Users.Update(user);
            _context.SaveChanges();
            return new
            {
                Success = true,
                Message = "update successfully!",
                Status = StatusCodes.Status200OK,
                user
            };
        }

        public object Create(User t)
        {
            throw new NotImplementedException();
        }

        public object Update(int id, User t)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// new token
        /// </summary>
        /// <param name="tokenModel"></param>
        /// <returns></returns>
        public object Create(TokenModel tokenModel)
        {
            var iwtTokenHandler = new JwtSecurityTokenHandler();
            var secretKyeBytes = Encoding.UTF8.GetBytes(_appSettings.SecretKey);
            var tokenvalidateParam = new TokenValidationParameters
            {

                //tự cấp token
                ValidateIssuer = false,
                ValidateAudience = false,
                //ko kiểm tra hết hạn
                ValidateLifetime = false,

                //Ký vào token
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(secretKyeBytes),

                ClockSkew = TimeSpan.Zero
            };
            try
            {
                // check 1: Accesstoken valid format
                var tokenInVerification = iwtTokenHandler.ValidateToken(tokenModel.AccsessToken, tokenvalidateParam, out var validatedToken);

                // check 2: check alg
                if (validatedToken is JwtSecurityToken jwtSecurityToken)
                {
                    var result = jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha512, StringComparison.InvariantCultureIgnoreCase);
                    if (!result)
                    {
                        return new
                        {
                            Success = false,
                            Message = "InValid Token!",
                        };

                    }
                }

                // check 3: check accessToken expire?
                var utcExpireDate = long.Parse(tokenInVerification.Claims.FirstOrDefault(
                    x => x.Type == JwtRegisteredClaimNames.Exp).Value);
                var expireDate = ConvertUnixTimeToDate(utcExpireDate);
                if (expireDate > DateTime.Now)
                {
                    return new
                    {
                        Success = false,
                        Message = "AccessToken has not yet expired!",
                    };
                }

                //check 4: check refreshTken exist in DB
                var storedToken = _context.RefreshTokens.FirstOrDefault(x => x.Token == tokenModel.RefreshToken);
                if (storedToken == null)
                {
                    return new
                    {
                        Success = false,
                        Message = "RefreshToken dose not exist in DB!",
                    };
                }

                //check 5: check is used/revoked?
                if (storedToken.IsUsed)
                {
                    return new
                    {
                        Success = false,
                        Message = "RefreshToken has been Used!",
                    };
                }
                if (storedToken.IsRevoked)
                {
                    return new
                    {
                        Success = false,
                        Message = "RefreshToken has been Revoked!",
                    };
                }

                //check 6: AccessToken id == JwtId in RefreshToken 
                var jti = tokenInVerification.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                if (storedToken.JwtId != jti)
                {
                    return new
                    {
                        Success = false,
                        Message = "Token dose not match!",
                    };
                }

                // update Token is used
                storedToken.IsRevoked = true;
                storedToken.IsUsed = true;
                _context.Update(storedToken);
                _context.SaveChangesAsync();

                //create new Token 
                var user = _context.Users.SingleOrDefault(u => u.Id == storedToken.UserId);
                var token = GenerateToken(user);

                return new
                {
                    Success = true,
                    Message = "Renew token success!",
                    Data = token

                };
            }
            catch
            {
                return new
                {
                    Success = false,
                    Message = "Something went wrong!",
                };
            }
        }

        public object Update(int id, TokenModel t)
        {
            throw new NotImplementedException();
        }

        private DateTime ConvertUnixTimeToDate(long utcExpireDate)
        {
            var dateTimeInterval = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dateTimeInterval.AddSeconds(utcExpireDate).ToUniversalTime();
            return dateTimeInterval;
        }
    }
}
