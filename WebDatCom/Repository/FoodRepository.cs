﻿using WebDatCom.Data;
using WebDatCom.Models;

namespace WebDatCom.Repository
{
    public class FoodRepository : IRepository<ModelFood>
    {
        private readonly OrderRiceDbContext _context;

        public FoodRepository(OrderRiceDbContext context)
        {
            _context = context;
        }

        public object Create(ModelFood model)
        {
            var _food = new Food
            {
                Description = model.Description,
                FoodName = model.FoodName,
                Category = model.Category,
                Price = model.Price,
            };
            _context.Foods.Add(_food);
            _context.SaveChanges();
            return _food;
        }

        public object Get(int id)
        {
            var food = _context.Foods.SingleOrDefault(x => x.Id == id);
            return food;
        }

        public IEnumerable<object> GetAll()
        {
            //var food = _context.Foods.Select(f => new { f.Description, f.FoodName, f.Price });
            return _context.Foods.ToList();
        }

        public object Delete(int id)
        {
            var food = _context.Foods.SingleOrDefault(x => x.Id == id);
            if (food == null)
            {
                return BadHttpRequestException.ReferenceEquals;
            }
            _context.Foods.Remove(food);
            _context.SaveChanges();
            return food;
        }

        public object Update(int id, ModelFood model)
        {
            var food = _context.Foods.SingleOrDefault(x => x.Id == id);
            if (food == null)
            {
                return BadHttpRequestException.ReferenceEquals;
            }
            food.Description = model.Description;
            food.FoodName = model.FoodName;
            food.Price = model.Price;

            _context.Foods.Update(food);
            _context.SaveChanges();
            return food;
        }

    }
}
