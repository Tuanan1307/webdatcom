﻿using Microsoft.EntityFrameworkCore;
using WebDatCom.Data;
using WebDatCom.Models;

namespace WebDatCom.Repository
{
    public class OrderDetailRepository : IRepository<ModelOrderDetail>
    {

        private readonly OrderRiceDbContext _context;

        public OrderDetailRepository(OrderRiceDbContext context)
        {
            _context = context;
        }
        public object Create(ModelOrderDetail model)
        {
            var orderDetail = new OrderDetail
            {
                FoodId = model.FoodId,
                // Food = null,
                OrderId = model.OrderId,
                // Order = null
            };
            _context.OrderDetails.Add(orderDetail);
            _context.SaveChanges();
            return orderDetail;
        }

        public object Delete(int id)
        {
            throw new NotImplementedException();
        }

        public object Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<object> GetAll()
        {

            return _context.OrderDetails.Include(x => x.Order).Include(x => x.Food).ToList();
        }

        public object Update(int id, ModelOrderDetail t)
        {
            throw new NotImplementedException();
        }


    }
}
