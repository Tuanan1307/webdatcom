﻿namespace WebDatCom.Repository
{
    public interface IRepository<T>
    {
        IEnumerable<Object> GetAll();
        Object Get(int id);
        Object Create(T t);
        Object Update(int id, T t);
        Object Delete(int id);

    }
}
