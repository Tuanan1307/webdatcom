﻿using Microsoft.EntityFrameworkCore;
using WebDatCom.Data;
using WebDatCom.Models;

namespace WebDatCom.Repository
{
    public class OrderRepository : IRepository<ModelOrder>
    {
        private readonly OrderRiceDbContext _context;

        public OrderRepository(OrderRiceDbContext context)
        {
            _context = context;
        }

        public object Create(ModelOrder model)
        {
            var order = new Order
            {
                Note = model.Note,
                CreatedAt = model.CreatedAt,
                TotalMoney = model.TotalMoney,
                UserId = model.UserId
            };
            _context.Orders.Add(order);
            _context.SaveChanges();
            return order;
        }

        public object Delete(int id)
        {
            throw new NotImplementedException();
        }

        public object Get(int id)
        {
            var order = _context.Orders.Where(x => x.Id == id).Include(x => x.User).Include(x => x.OrderDetails).SingleOrDefault();
            return order;
        }

        public IEnumerable<object> GetAll()
        {
            var orders = _context.Orders.Include(x => x.User).Include(x => x.OrderDetails).ToList();
            return orders;
        }

        public object Update(int id, ModelOrder t)
        {
            throw new NotImplementedException();
        }
    }
}
